package com.czq.nest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NestBDTO {
    private int inta;
    private Short intB;
    private String stringc;
    private NestA2DTO nestA2DTO;
    private String stringe;
}
