package com.czq.nest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NestADTO {
    private int inta;
    private Integer intb;
    private String stringc;
    private NestA2DTO nestA2DTO;
    private NestA3DTO nestA3DTO;
}
