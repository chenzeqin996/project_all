package com.czq.convert;

import com.czq.first.dto.ADTO;
import com.czq.first.dto.BDTO;
import com.czq.nest.dto.NestADTO;
import com.czq.nest.dto.NestBDTO;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

// @Mapper
@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
        ,nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface Convert {

    Convert INS = Mappers.getMapper(Convert.class);

    BDTO adto2b(ADTO a);

    void adto2b(ADTO a,@MappingTarget BDTO b);

    @Mapping(source = "nestA3DTO.stringe", target = "stringe")
    NestBDTO nestADTO2b(NestADTO a);
}
