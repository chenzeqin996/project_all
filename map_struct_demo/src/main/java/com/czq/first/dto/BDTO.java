package com.czq.first.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BDTO {
    private int inta;
    private Short intb;
    private String Stringc;
}
