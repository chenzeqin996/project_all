import com.czq.convert.Convert;
import com.czq.first.dto.ADTO;
import com.czq.first.dto.BDTO;
import com.czq.nest.dto.NestA2DTO;
import com.czq.nest.dto.NestA3DTO;
import com.czq.nest.dto.NestADTO;
import com.czq.nest.dto.NestBDTO;
import org.junit.Test;

public class DemoTest {

    @Test
    public void test01(){
        ADTO adto = new ADTO(1, 2, "3");
        BDTO bdto = Convert.INS.adto2b(adto);
        System.out.println(bdto);
    }

    @Test
    public void test02(){
        NestA2DTO a2DTO = new NestA2DTO("4");
        NestA3DTO a3DTO = new NestA3DTO("5");
        NestADTO adto = new NestADTO(1, 2, "3", a2DTO, a3DTO);
        NestBDTO nestBDTO = Convert.INS.nestADTO2b(adto);
        System.out.println(nestBDTO);
    }
}
