<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 需要加这个，不然el表达式没有解析出来 --%>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
你好<br>
${userList}
<c:forEach items="${userList}" var="user">
    ${user.id}---${user.name}<br>
</c:forEach>
</body>
</html>
