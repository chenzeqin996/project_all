package com.chenzeqin.user.dao;

import com.chenzeqin.user.entity.User;
import java.util.List;

public interface UserMapper {

    List<User> findAll();
}
