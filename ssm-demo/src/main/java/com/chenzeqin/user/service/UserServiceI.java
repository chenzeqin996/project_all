package com.chenzeqin.user.service;


import com.chenzeqin.user.entity.User;
import java.util.List;

public interface UserServiceI {

    List<User> findAll();
}
