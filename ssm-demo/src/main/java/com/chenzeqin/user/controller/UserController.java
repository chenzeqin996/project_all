package com.chenzeqin.user.controller;

import com.chenzeqin.user.entity.User;
import com.chenzeqin.user.service.UserServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserServiceI userServiceI;

    @RequestMapping("/hello")
    @ResponseBody
    public String hello(){
        return "hello";
    }

    @RequestMapping("/findAll")
    public String findAll(Model model){
        List<User> list = userServiceI.findAll();
        model.addAttribute("userList", list);
        return "/user/user-list";
    }
}
