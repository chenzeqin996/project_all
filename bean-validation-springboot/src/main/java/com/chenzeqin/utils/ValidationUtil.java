package com.chenzeqin.utils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author: chenzeqin
 * @date: 2022/4/4
 */
public class ValidationUtil {
    private static Validator validator;

    static{
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    public static <T> List<String> valid(T object){
        Set<ConstraintViolation<T>> vset = validator.validate(object);
        List<String> result = vset.stream().map(x ->{
            String msg = "属性："+x.getPropertyPath()+"，属性的值："+x.getInvalidValue()+"，校验不通过的提示信息:"+x.getMessage()
                                 + "，消息模板："+x.getMessageTemplate();
            System.out.println(msg);
            return msg;
        }).collect(Collectors.toList());
        return result;
    }
}
