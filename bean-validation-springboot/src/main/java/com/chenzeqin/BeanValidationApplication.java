package com.chenzeqin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: chenzeqin
 * @date: 2022/4/4
 */
@SpringBootApplication
public class BeanValidationApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeanValidationApplication.class, args);
    }
}
