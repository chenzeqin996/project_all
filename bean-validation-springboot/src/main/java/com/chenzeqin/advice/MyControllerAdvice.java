package com.chenzeqin.advice;

import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author: chenzeqin
 * @date: 2022/4/4
 */
@ControllerAdvice
public class MyControllerAdvice {

    /**
     * 统一异常处理
     * 针对所有controller抛出的异常
     * @Valid注解写在方法上的时候报这个异常
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(BindException.class)// 指定要处理的异常
    public String handleEx(BindException e){
        StringBuffer sb = new StringBuffer("MyControllerAdvice ");
        List<FieldError> fieldErrors = e.getFieldErrors();
        fieldErrors.forEach(f ->{
            sb.append("校验不通过属性：").append(f.getField());
            sb.append(", 原因：").append(f.getDefaultMessage());
            sb.append("; ");
        });
        return sb.toString();
    }

    /**
     * @Validate注解写在类上的时候报这个异常
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)// 指定要处理的异常
    public List<String> handleEx(ConstraintViolationException e){
        StringBuffer sb = new StringBuffer("MyControllerAdvice ");
        Set<ConstraintViolation<?>> vset = e.getConstraintViolations();
        List<String> result = vset.stream().map(x ->{
            String msg = "属性："+x.getPropertyPath()+"，属性的值："+x.getInvalidValue()+"，校验不通过的提示信息:"+x.getMessage()
                                 + "，消息模板："+x.getMessageTemplate();
            System.out.println(msg);
            return msg;
        }).collect(Collectors.toList());
        return result;
    }
}
