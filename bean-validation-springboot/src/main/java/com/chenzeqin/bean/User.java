package com.chenzeqin.bean;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * @author: chenzeqin
 * @date: 2022/4/4
 */
@Data
public class User {
    // 新增两个内部接口，用于分组
    public interface Add{}
    public interface Update{}

    // groups不写的话，默认分配到组javax.validation.groups.Default.class
    @Null(groups = Add.class)
    @NotNull(groups = Update.class)
    private Long id;

    @NotNull
    private String name;
}
