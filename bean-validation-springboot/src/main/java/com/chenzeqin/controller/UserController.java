package com.chenzeqin.controller;

import com.chenzeqin.bean.User;
import com.chenzeqin.utils.ValidationUtil;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.util.List;

/**
 * @author: chenzeqin
 * @date: 2022/4/4
 */
@Validated  // @Validated支持加载类上，表示整个类都启用校验，如果碰到方法入参加有约束注解的话，就会自动校验
@RestController
public class UserController {

    @RequestMapping("/getById")
    public String getById(@NotNull Long id){
        return "getById:"+id;
    }

    /**
     * 编程式校验
     * @param user
     * @return
     */
    @RequestMapping("addUser")
    public String addUser(User user){
        // 编程式校验
        List<String> valid = ValidationUtil.valid(user);
        if(!CollectionUtils.isEmpty(valid)){
            return "校验不通过";
        }else{
            return "保存成功";
        }
    }

    /**
     * 声明式校验-直接报错
     * @param user
     * @return
     */
    @RequestMapping("addUser2")
    public String addUser2(@Valid User user){
        // 如果校验不通过的话页面会直接报错
        return "保存成功";
    }

    /**
     * 声明式校验-获取报错信息
     * @param user
     * @return
     */
    @RequestMapping("addUser3")
    public String addUser3(@Valid User user, BindingResult bindingResult){
        // 通过BindingResult获取校验结果
        if(bindingResult.hasErrors()){
            List<ObjectError> allErrors = bindingResult.getAllErrors();
            allErrors.forEach(x ->{
                System.out.println(x);
            });
            // 获取没通过校验的字段详情
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            fieldErrors.forEach(y ->{
                System.out.println(y);
            });
            return "校验不通过";
        }else{
            return "保存成功";
        }
    }


    @RequestMapping("groupAddUser")
    public String groupAddUser(@Validated(value = {User.Add.class, Default.class}) User user){
        return "保存成功";
    }

    @RequestMapping("groupUpdateUser")
    public String groupUpdateUser(@Validated(value = {User.Update.class, Default.class}) User user){
        return "保存成功";
    }

    /**
     * 统一异常处理
     * 在每个controller里面写上@ExceptionHandler(xxx.class)，可以处理当前controller里面抛出的xxx异常
     * @param e
     * @return
     */
    // @ExceptionHandler(BindException.class)// 指定要处理的异常
    // public String handleEx(BindException e){
    //     StringBuffer sb = new StringBuffer();
    //     List<FieldError> fieldErrors = e.getFieldErrors();
    //     fieldErrors.forEach(f ->{
    //         sb.append("校验不通过属性：").append(f.getField());
    //         sb.append(", 原因：").append(f.getDefaultMessage());
    //         sb.append("; ");
    //     });
    //     return sb.toString();
    // }



}
