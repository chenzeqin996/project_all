package com.chenzeqin.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: chenzeqin
 * @date: 2022/4/4
 */
@RestController
public class TestController {

    @RequestMapping("/hello")
    public String hello(String name){
        return "hello "+name;
    }
}
