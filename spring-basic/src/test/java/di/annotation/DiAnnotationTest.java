package di.annotation;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DiAnnotationTest{

	@Test
	public void test(){
		ApplicationContext context = new ClassPathXmlApplicationContext("di/annotation/applicationContext-di-annotation.xml");
		Person person = (Person)context.getBean("person_anno");
		person.showStudent();
	}
}
