package beanLifeCycle;

import beanLifeCycle.five.Dog;
import beanLifeCycle.seven.Cat;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * bean的生命周期测试
 */
public class beanLifeCycleTest {

    /**
     * bean的生命周期测试-5步骤版本
     */
    @Test
    public void fiveTest(){
        ApplicationContext context = new ClassPathXmlApplicationContext("beanLifeCycle/beanLifeCycleFiveContext.xml");
        Dog dog = context.getBean(Dog.class);
        System.out.println("4.使用bean，获取实例name值："+dog.getName());
        ((ClassPathXmlApplicationContext) context).close();
    }

    /**
     * bean的生命周期测试-7步骤版本
     */
    @Test
    public void sevenTest(){
        ApplicationContext context = new ClassPathXmlApplicationContext("beanLifeCycle/beanLifeCycleSevenContext.xml");
        Cat cat = context.getBean(Cat.class);
        System.out.println("4.使用bean，获取实例name值："+cat.getName());
        ((ClassPathXmlApplicationContext) context).close();
    }
}
