package beanLifeCycle.seven;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor implements BeanPostProcessor {
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if("cat".equals(beanName)){
            System.out.println("beanPostProcessor before方法");
        }
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if("cat".equals(beanName)){
            System.out.println("beanPostProcessor after方法");
        }
        return bean;
    }
}
