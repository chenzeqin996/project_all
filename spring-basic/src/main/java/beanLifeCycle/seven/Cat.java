package beanLifeCycle.seven;

/**
 * 用于测试bean的生命周期：五步骤版本
 */
public class Cat {
    private String name;

    public Cat(){
        System.out.println("1.实例化（调用构造函数）");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        System.out.println("2.依赖注入（调用set方法进行属性注入）");
    }

    public void myinit(){
        System.out.println("3.初始化bean（调用自定义的初始化方法）");
    }

    public void myDestroy(){
        System.out.println("5.销毁bean（调用自定义的销毁方法）");
    }
}
