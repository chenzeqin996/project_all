package factoryBean;/**
 * @author chenzeqin
 * @date 2023/8/15
 */

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;

/**
 *
 * @author chenzeqin
 * @date 2023/8/15
 */
@Configurable
public class MainConfig {

    @Bean
    public Car car(){
        return new Car("奔驰");
    }
}
