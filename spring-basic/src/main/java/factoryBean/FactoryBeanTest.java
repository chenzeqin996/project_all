package factoryBean;/**
 * @author chenzeqin
 * @date 2023/8/15
 */

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

/**
 * 该事例用于测试FactoryBean的使用方式
 * @author chenzeqin
 * @date 2023/8/15
 */
public class FactoryBeanTest {

    @Test
    public void test1(){
        ApplicationContext context = new AnnotationConfigApplicationContext(MainConfig.class);
        Car car = (Car) context.getBean("car");
        System.out.println(car);
        // 输出结果：Car(carType=宝马)
        // 结论：如果类实现了FactoryBean，则spring不会在用反射的方式获取bean,而是直接用FactoryBean.getObject()方法去获取bean
    }
}
