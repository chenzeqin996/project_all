package factoryBean;/**
 * @author chenzeqin
 * @date 2023/8/15
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

/**
 *
 * @author chenzeqin
 * @date 2023/8/15
 */
// @Component
@Data
@AllArgsConstructor
public class Car implements FactoryBean {
    private String carType;

    // 实现FactoryBean重写-创建对象
    public Object getObject() throws Exception {
        Car car = new Car("宝马");
        return car;
    }

    // 实现FactoryBean重写-设置返回的对象类型
    public Class<?> getObjectType() {
        return Car.class;
    }

    // 实现FactoryBean重写-是否是单例
    public boolean isSingleton() {
        return true;
    }
}
