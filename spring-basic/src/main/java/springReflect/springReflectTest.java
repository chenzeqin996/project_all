package springReflect;

import org.junit.Test;

import java.lang.reflect.Field;

/**
 * spring注解方式注入bean
 * @author chenzeqin
 * @date 2023/10/7
 */
public class springReflectTest {

    /**
     * springbean的使用反正模拟注解注入对象
     * @author chenzeqin
     * @date 2023/10/7
     */
    @Test
    public void test() throws Exception {
        UserController userController = new UserController();
        Class<UserController> clazz = UserController.class;
        // 获取属性
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            // 获取自定义注解
            MyAutowired myAutowired = declaredField.getAnnotation(MyAutowired.class);
            // 如果属性带有自定义注解，则注入对象
            if(myAutowired!=null){
                declaredField.setAccessible(true);
                Class<?> type = declaredField.getType();
                Object o = type.newInstance();
                // 注入
                declaredField.set(userController, o);
            }
        }
        System.out.println(userController.getUserService());
    }
}
