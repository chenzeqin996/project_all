package springReflect;

import java.lang.annotation.*;

/**
 * @author chenzeqin
 * @date 2023/10/7
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface MyAutowired {
}
