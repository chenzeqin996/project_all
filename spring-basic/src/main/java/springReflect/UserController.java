package springReflect;



/**
 *
 * @author chenzeqin
 * @date 2023/10/7
 */
public class UserController {

    @MyAutowired
    private UserService userService;

    public UserService getUserService() {
        return userService;
    }
}
