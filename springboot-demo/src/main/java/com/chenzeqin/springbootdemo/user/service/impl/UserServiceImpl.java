package com.chenzeqin.springbootdemo.user.service.impl;

import com.chenzeqin.springbootdemo.user.dao.UserMapper;
import com.chenzeqin.springbootdemo.user.entity.UserEntity;
import com.chenzeqin.springbootdemo.user.service.UserServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class UserServiceImpl implements UserServiceI {
    @Autowired
    private UserMapper userMapper;

    public List<UserEntity> findAll() {
        return userMapper.findAll();
    }

    public UserEntity findById(Integer id) {
        return userMapper.findById(id);
    }
}
