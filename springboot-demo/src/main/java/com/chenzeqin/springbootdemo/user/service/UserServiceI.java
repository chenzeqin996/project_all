package com.chenzeqin.springbootdemo.user.service;

import com.chenzeqin.springbootdemo.user.entity.UserEntity;
import java.util.List;

public interface UserServiceI {
    List<UserEntity> findAll();

    UserEntity findById(Integer id);
}
