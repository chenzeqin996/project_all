package com.chenzeqin.springbootdemo.user.dao;

import com.chenzeqin.springbootdemo.user.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

// @Mapper
// 在启动类上写上了@MapperScan，已经可以扫描到此接口，无需在接口上重复写 @Mapper
public interface UserMapper {
    List<UserEntity> findAll();

    UserEntity findById(Integer id);

}
