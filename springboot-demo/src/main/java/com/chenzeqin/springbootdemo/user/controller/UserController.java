package com.chenzeqin.springbootdemo.user.controller;

import com.chenzeqin.springbootdemo.user.entity.UserEntity;
import com.chenzeqin.springbootdemo.user.service.UserServiceI;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserServiceI userServiceI;

    @GetMapping("/findAll")
    public List<UserEntity> findAll(){
        log.info("执行了findAll");
        List<UserEntity> list = userServiceI.findAll();
        return list;
    }

    @GetMapping("/findById")
    public UserEntity findById(Integer id){
        UserEntity entity = userServiceI.findById(id);
        return entity;
    }
}
