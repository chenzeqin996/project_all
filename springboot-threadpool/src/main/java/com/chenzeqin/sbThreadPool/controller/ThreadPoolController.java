package com.chenzeqin.sbThreadPool.controller;

import com.chenzeqin.sbThreadPool.service.ThreaPoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ThreadPoolController {

    @Autowired
    private ThreaPoolService threaPoolService;

    @GetMapping("asyncThreadPool")
    public String asyncThreadPool(){
        threaPoolService.testDefaultSpringThreadPool();
        threaPoolService.testMyThreadPool();
        threaPoolService.testMyThreadPool2();
        return "success";
    }
}
