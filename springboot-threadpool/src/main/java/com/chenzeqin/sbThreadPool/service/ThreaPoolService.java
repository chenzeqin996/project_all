package com.chenzeqin.sbThreadPool.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.util.concurrent.Executor;


@Service
@EnableAsync// @EnableAsync启动多线程注解
public class ThreaPoolService {
    @Autowired
    private Executor taskExector1;

    @Async  // @Async就会对标注的方法开启异步多线程调用
    public void testDefaultSpringThreadPool(){
        try {
            Thread.sleep(2000);
            System.out.println("Spring1自带的线程池" + Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 线程池使用方式一：注解形式使用，taskExector1为bean的id
     */
    @Async("taskExector1")
    public void testMyThreadPool(){
        try {
            Thread.sleep(2000);
            System.out.println("注解方式启动自定义的线程池" + Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 线程池使用方式二：注入编码方式使用线程池
     */
    public void testMyThreadPool2(){
        taskExector1.execute(() ->{
            try {
                Thread.sleep(2000);
                System.out.println("编码方式启动自定义的线程池" + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
