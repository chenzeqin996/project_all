package com.chenzeqin.sbThreadPool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbThreadPoolApp {

    public static void main(String[] args) {
        SpringApplication.run(SbThreadPoolApp.class, args);
    }
}
