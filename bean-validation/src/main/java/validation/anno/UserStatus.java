package validation.anno;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Accepts Integer
 */
@Target({FIELD})
@Retention(RUNTIME)
@Documented
// 将约束注解和校验器绑定起来
@Constraint(validatedBy = {UserStatusValidator.class})
public @interface UserStatus {
    // 默认错误的提示信息
    String message() default "{status的状态值错误}";

    // 默认的组
    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
