package validation.anno;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

/**
 * @author: chenzeqin
 * @date: 2022/4/3
 */
public class UserStatusValidator implements ConstraintValidator<UserStatus, Object> {

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        if(object == null){
            return true;
        }
        Set<Integer> set = new HashSet<>();
        set.add(100);
        set.add(101);
        set.add(102);
        // 校验status的值，只能是100/101/102
        return set.contains(object);
    }
}
