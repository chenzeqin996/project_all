package validation.service;

import validation.User;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.validation.executable.ExecutableValidator;
import java.lang.reflect.Method;
import java.util.Set;

/**
 * @author: chenzeqin
 * @date: 2022/4/4
 */
public class UserService {

    /***
     * 非bean类型的入参校验
     * 1、方法参数前加约束注解 @NotNull
     * 2、执行入参校验
     */
    public String getById(@NotNull Long id) throws NoSuchMethodException {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        ExecutableValidator executableValidator = validator.forExecutables();
        // 获取当前方法Method对象
        StackTraceElement st = Thread.currentThread().getStackTrace()[1];
        Method method = this.getClass().getDeclaredMethod(st.getMethodName(), Long.class);
        // 执行入参校验
        Set<ConstraintViolation<UserService>> vset = executableValidator.validateParameters(this, method, new Object[]{id});
        vset.forEach(x ->{
            System.out.println("属性："+x.getPropertyPath()+"，属性的值："+x.getInvalidValue()+"，校验不通过的提示信息:"+x.getMessage()
                    + "，消息模板："+x.getMessageTemplate());
        });

        return "sucess";
    }
}
