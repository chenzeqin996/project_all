package validation;

import org.hibernate.validator.HibernateValidator;
import validation.service.UserService;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.util.Set;

/**
 * @author: chenzeqin
 * @date: 2022/4/3
 */
public class ValidationTest {

    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public static void main(String[] args) {
        User u = new User();
        u.setAge(17);
        // 校验对象，如果校验对象没有校验通过，则set将返回不同过的校验错误信息
        Set<ConstraintViolation<User>> vset = validator.validate(u);
        vset.forEach(x ->{
            //System.out.println(x);
            System.out.println("属性："+x.getPropertyPath()+"，属性的值："+x.getInvalidValue()+"，校验不通过的提示信息:"+x.getMessage()
                    + "，消息模板："+x.getMessageTemplate());
            //getMessageTemplate消息模板是获取到EL表达式中未被替换成对应值时候的内容
        });

    }

    /***
     * 分组校验
     * @param args
     */
//    public static void main(String[] args) {
//        User u = new User();
//        u.setAge(17);
//        // 校验时，最好写上Default.class，不加上的话就不校验Default组,因为一般约束注解不写组的时候都是放在Default组里的。
//        Set<ConstraintViolation<User>> vset = validator.validate(u, User.Update.class, Default.class);
//        vset.forEach(x ->{
//            System.out.println("属性："+x.getPropertyPath()+"，属性的值："+x.getInvalidValue()+"，校验不通过的提示信息:"+x.getMessage()
//                    + "，消息模板："+x.getMessageTemplate());
//        });
//    }

//    /**
//     * 级联校验
//     * @param args
//     */
//    public static void main(String[] args) {
//        User u = new User();
//        u.setName("chen");
//        u.setAge(18);
//        u.setGrade(new Grade());
//        Set<ConstraintViolation<User>> vset = validator.validate(u);
//        vset.forEach(x ->{
//            System.out.println("属性："+x.getPropertyPath()+"，属性的值："+x.getInvalidValue()+"，校验不通过的提示信息:"+x.getMessage()
//                    + "，消息模板："+x.getMessageTemplate());
//        });
//    }

    /**
      * 自定义校验规则
      * @param args
      */
//    public static void main(String[] args) {
//        User u = new User();
//        u.setName("chen");
//        u.setAge(18);
//        u.setStatus(0);
//        Set<ConstraintViolation<User>> vset = validator.validate(u);
//        vset.forEach(x ->{
//            System.out.println("属性："+x.getPropertyPath()+"，属性的值："+x.getInvalidValue()+"，校验不通过的提示信息:"+x.getMessage()
//                    + "，消息模板："+x.getMessageTemplate());
//        });
//    }

    // 配置快速失败校验器
    private static Validator failFastValidator = Validation.byProvider(HibernateValidator.class)
            .configure().failFast(true)
            .buildValidatorFactory().getValidator();

    /**
     * 快速失败校验器
     * @param args
     */
//    public static void main(String[] args) {
//        User u = new User();
//        u.setName("chen");
//        u.setAge(18);
//        u.setStatus(0);
//        Set<ConstraintViolation<User>> vset = failFastValidator.validate(u);
//        vset.forEach(x ->{
//            System.out.println("属性："+x.getPropertyPath()+"，属性的值："+x.getInvalidValue()+"，校验不通过的提示信息:"+x.getMessage()
//                    + "，消息模板："+x.getMessageTemplate());
//        });
//    }

    /**
     * 非bean入参校验
     * @param args
     * @throws NoSuchMethodException
     */
//    public static void main(String[] args) throws NoSuchMethodException {
//        UserService userService = new UserService();
//        userService.getById(null);
//    }
}
