package validation;

import lombok.Data;
import validation.anno.UserStatus;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.groups.Default;
import java.time.LocalDateTime;

/**
 * @author: chenzeqin
 * @date: 2022/4/3
 */
@Data
public class User {
    // 新增两个内部接口，用于分组
    public interface Add{}
    public interface Update{}

    // groups不写的话，默认分配到组javax.validation.groups.Default.class
    @Null(groups = Add.class)
    @NotNull(groups = Update.class)
    private Long id;

    // 非空校验注解
    @NotNull(message = "不能为空", groups = Default.class)
    private String name;

    @Min(value = 18, message = "年龄必须大于等于{value}岁")
    private Integer age;

    //@NotBlank   // 看一个注解可以作用在哪些类型的字段上，直接点进去看源码开头的注释就可以了
    private LocalDateTime birthday;

    @NotNull
    @Valid  // 开启级联校验
    private Grade grade;

    @UserStatus
    private Integer status;
}
