package validation.utils;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.executable.ExecutableValidator;

/**
 * @author: chenzeqin
 * @date: 2022/4/4
 */
public class ValidationUtil {
    private static Validator validator;
    private static ExecutableValidator executableValidator;

    static{
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        executableValidator = validator.forExecutables();
    }


}
