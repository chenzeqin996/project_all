import com.chenzeqin.aware.User;
import com.chenzeqin.bean.Person;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author chenzeqin
 * date 2020/7/11
 */
public class Test {

    @org.junit.Test
    public void test01(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Person person = (Person) context.getBean("person");
        System.out.println(person);
    }

    @org.junit.Test
    public void awareTest(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        User user = (User) context.getBean("user");
        System.out.println("beanName="+user.getBeanName());
        System.out.println("applicationContext="+user.getApplicationContext());
    }
}
