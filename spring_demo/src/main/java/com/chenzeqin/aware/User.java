package com.chenzeqin.aware;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

public class User implements BeanNameAware, ApplicationContextAware {
    private String id;
    private String name;
    // 使用BeanNameAware注入beanName属性
    private String beanName;
    // 使用ApplicationContextAware注入applicationContext
    private ApplicationContext applicationContext;

    public void setBeanName(String s) {
        System.out.println("BeanNameAware调用get方法注入beanName属性");
        this.beanName = s;
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("ApplicationContextAware调用get方法注入applicationContext属性");
        this.applicationContext = applicationContext;
    }

    public String getBeanName() {
        return beanName;
    }

    public ApplicationContext getApplicationContext(){
        return applicationContext;
    }
}
