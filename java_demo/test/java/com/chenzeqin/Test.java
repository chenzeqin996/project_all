package com.chenzeqin;

import sun.misc.Unsafe;

/**
 * @author chenzeqin
 * @menu
 * @date 2021-1-9
 */
public class Test {

    public static void main(String[] args) {
        Integer i = 127;
        Integer j = 127;
        System.out.println(i == j );
        Integer i1 = new Integer(1);
        Integer j1 = new Integer(1);
        System.out.println(i1 == j1);
    }
}
