package com.chenzeqin.java8.java8DateUtil;


import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * 参考：https://mp.weixin.qq.com/s/DzwO8NnQObWFmB8gQ97lyA
 * Java8 是目前企业中使用的最主流版本，它升级了日期API，很多人还不会用，今天我们一起来学习它的一些优秀实践案例！
 * Java8 推出了全新的日期时间API，在教程中我们将通过一些简单的实例来学习如何使用新API。
 * Java处理日期、日历和时间的方式一直为社区所诟病，将 java.util.Date设定为可变类型，以及SimpleDateFormat的非线程安全使其应用非常受限。
 * 新API基于ISO标准日历系统，java.time包下的所有类都是不可变类型而且线程安全。
 * @author chenzeqin
 * @date 2021-1-19
 */
public class DateTest {
    @Test
    public void getNow1() throws ParseException {
        String str = "2021-12-03 06:00:00";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse = sdf.parse(str);
        long time = parse.getTime();

        long nowtime = System.currentTimeMillis();

        System.out.println(nowtime - time);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime nextDate = now.plusDays(1);
        LocalDateTime autoUpdateTime1 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 6, 0);
        LocalDateTime autoUpdateTime2 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 11, 00);
        LocalDateTime autoUpdateTime3 = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 18, 20);
        LocalDateTime autoUpdateTime4 = LocalDateTime.of(nextDate.getYear(), nextDate.getMonth(), nextDate.getDayOfMonth(), 6, 0);
        System.out.println(nextDate);
        Duration duration1 = Duration.between(now, autoUpdateTime1);
        Duration duration2 = Duration.between(now, autoUpdateTime2);
        Duration duration3 = Duration.between(now, autoUpdateTime3);
        Duration duration4 = Duration.between(now, autoUpdateTime4);
        System.out.println(duration1.toHours());
        System.out.println(duration2.toMinutes());
        System.out.println(duration3.toHours());
        System.out.println(duration4.toHours());

    }
    /**
     * 获取当前时间,自定义时间
     */
    @Test
    public void getNow(){
        // 获取今天日期
        LocalDate today = LocalDate.now();
        System.out.println("获取当前的时间,不含时分秒："+today);
        // 获取当前时间的HH:mm:ss.SSS 时分秒数据
        LocalTime time = LocalTime.now();
        System.out.println("获取当前的时间,不含有日期:"+time);
        // 获取当前时间
        LocalDateTime now = LocalDateTime.now();
        System.out.println("获取当前的时间,含有日期时间:"+now);

        // 获取年、月、日信息
        int year = today.getYear();
        int month = today.getMonthValue();
        int day = today.getDayOfMonth();
        System.out.println("year:"+year);
        System.out.println("month:"+month);
        System.out.println("day:"+day);

        /**
         * 我们通过静态工厂方法now()非常容易地创建了当天日期，你还可以调用另一个有用的工厂方法LocalDate.of()创建任意日期，
         * 该方法需要传入年、月、日做参数，返回对应的LocalDate实例。
         * 这个方法的好处是没再犯老API的设计错误，比如年度起始于1900，月份是从0开 始等等。
         */
        LocalDate date = LocalDate.of(2020,12,12);
        System.out.println("自定义日期："+date);
    }

    /**
     * Java 8中判断两个日期是否相等
     */
    @Test
    public void checkDay(){
        LocalDate date1 = LocalDate.now();
        LocalDate date2 = LocalDate.of(2021,1,19);
        if(date1.equals(date2)){
            System.out.println("时间相等");
        }else{
            System.out.println("时间不等");
        }
    }



    /**
     * 检查像生日这种周期性事件
     */
    @Test
    public void checkBirthday(){
        LocalDate today = LocalDate.now();

        LocalDate birthday = LocalDate.of(2018,1,19);
        MonthDay birthdayMonthDay = MonthDay.of(birthday.getMonth(),birthday.getDayOfMonth());
        MonthDay currentMonthDay = MonthDay.from(today);

        if(currentMonthDay.equals(birthdayMonthDay)){
            System.out.println("是你的生日");
        }else{
            System.out.println("你的生日还没有到");
        }
    }


    /**
     * minus做减法，减去天、周、月、年数
     * plus做加法，增加天、周、月、年数
     * 或者可以直接使用plusYears()/plusDays()、minusYears()/minusDays()等方法来做加减
     * ChronoUnit 声明了这些时间单位
     */
    @Test
    public void dateCalcu(){
        LocalDate today = LocalDate.now();
        System.out.println("今天的日期为:"+today);
        // 年份加减
        LocalDate previousYear = today.minus(1, ChronoUnit.YEARS);
        System.out.println("一年前的日期 : " + previousYear);
        LocalDate nextYear = today.plus(1, ChronoUnit.YEARS);
        System.out.println("一年后的日期:"+nextYear);

        // 周加减
        LocalDate preWeek = today.minus(1, ChronoUnit.WEEKS);
        System.out.println("一周前的日期为 : " + preWeek);
        LocalDate nextWeek = today.plus(1, ChronoUnit.WEEKS);
        System.out.println("一周后的日期为:"+nextWeek);
    }


    /**
     * 判断日期是早于还是晚于另一个日期
     */
    @Test
    public void dateBeforeAfter(){
        LocalDate today = LocalDate.now();

        LocalDate tomorrow =today.plusDays(1);
        if(tomorrow.isAfter(today)){
            System.out.println("之后的日期:"+tomorrow);
        }

        LocalDate yesterday = today.minus(1, ChronoUnit.DAYS);
        if(yesterday.isBefore(today)){
            System.out.println("之前的日期:"+yesterday);
        }
    }

    /**
     * YearMonth 类
     * 与 MonthDay检查重复事件的例子相似，YearMonth是另一个组合类，用于表示信用卡到期日、FD到期日、期货期权到期日等。
     * 还可以用这个类得到 当月共有多少天，YearMonth实例的lengthOfMonth()方法可以返回当月的天数，在判断2月有28天还是29天时非常有用。
     */
    @Test
    public void yearMonth(){
        YearMonth currentYearMonth = YearMonth.now();
        System.out.printf("Days in month year %s: %d%n", currentYearMonth, currentYearMonth.lengthOfMonth());
        YearMonth creditCardExpiry = YearMonth.of(2019, Month.FEBRUARY);
        System.out.printf("Your credit card expires on %s %n", creditCardExpiry);
    }

    /**
     * 判断是否是闰年
     * 闰年判断条件：
     *      能被4整除，但不能被100整除的是闰年
     *      或者能被400整除的是闰年
     * 闰年来由：
     *      闰年(Leap Year)是为了弥补因人为历法规定造成的年度天数与地球实际公转周期的时间差而设立的。补上时间差的年份为闰年。
     *      闰年共有366天（2月份有29天），非闰年（即平年）有365天（2月份只有28天）
     */
    @Test
    public void isLeapYear(){
        LocalDate today = LocalDate.now();
        if(today.isLeapYear()){
            System.out.println(today.getYear()+"是闰年");
        }else {
            System.out.println(today.getYear()+"不是闰年");
        }
    }


    /**
     * Period 时间段类
     * 计算两个日期之间的天数和月数
     */
    @Test
    public void getPeriod(){
        LocalDate today = LocalDate.now();
        LocalDate java8Release = LocalDate.of(2022, 2, 19);
        // 这个方法计算的方式是月数相减
        // 例如2021-01-19和2022-02-19 调用period.getMonths()计算结果是2也就是月份相减，不管年。
        // 天数计算方式也跟上面是一样的
        Period period = Period.between(today, java8Release);
        System.out.println("计算两个日期相差天数 : " + period.getDays() );
        System.out.println("计算两个日期相差月数 : " + period.getMonths() );
    }


    /**
     * Instant 时间戳类
     * 获取时间戳
     *
     * 时间戳信息里同时包含了日期和时间，这和java.util.Date很像。
     * 实际上Instant类确实等同于 Java 8之前的Date类，
     * 你可以使用Date类和Instant类各自的转换方法互相转换，
     * 例如：Date.from(Instant) 将Instant转换成java.util.Date，
     * Date.toInstant()则是将Date类转换成Instant类。
     *
     * Clock 时钟类
     */
    @Test
    public void getTimestamp(){
        Instant timestamp = Instant.now();
        System.out.println("Instant方式获取时间戳："+timestamp.toEpochMilli());
        System.out.println("System方式获取时间戳："+System.currentTimeMillis());
        System.out.println("Date方式获取时间戳："+new Date().getTime());

        // Returns the current time based on your system clock and set to UTC.
        Clock clock = Clock.systemUTC();
        System.out.println("Clock方式获取时间戳 : " + clock.millis());
        // Returns time based on system clock zone 有点耗时
        Clock defaultClock = Clock.systemDefaultZone();
        System.out.println("Clock方式获取时间戳 : " + defaultClock.millis());

    }



    /**
     * DateTimeFormatter 时间格式化类
     * 字符串 日期 互转
     */
    @Test
    public void strAndDate(){
        LocalDateTime date = LocalDateTime.now();

        DateTimeFormatter format1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        //日期转字符串
        String str = date.format(format1);
        System.out.println("日期转换为字符串:"+str);
        //字符串转日期
        LocalDateTime date2 = LocalDateTime.parse(str,format1);
        System.out.println("字符串转日期类型:"+date2);

        //使用预定义的格式化工具去解析或格式化日期
        String dayAfterTommorrow = "20180205";
        LocalDate formatted = LocalDate.parse(dayAfterTommorrow, DateTimeFormatter.BASIC_ISO_DATE);
        System.out.println(dayAfterTommorrow+"  使用预定义的格式化工具，格式化后的日期为:  "+formatted);
    }


}
