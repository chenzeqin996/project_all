package com.chenzeqin.java8.lambda;

import com.chenzeqin.java8.functionalInterface.AFun;
import org.junit.Test;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.*;

/**
 * @author chenzeqin
 * @menu
 * @date 2021-6-29
 */
public class LambdaTest {

    @Test
    public void test1(){
        // 匿名内部类
        Comparator<Integer> cm = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };
        List<Integer> list = Arrays.asList(9, 50, 1, 20, 3);
        // 排序
        list.sort(cm);
    }

    @Test
    public void test2(){
        // lambda表达式
        Comparator<Integer> cm = (x, y) -> Integer.compare(x, y);
        List<Integer> list = Arrays.asList(9, 50, 1, 20, 3);
        // 排序
        list.sort(cm);
        // 如何理解 `Lambda 是一个匿名函数`，可以把 Lambda表达式 可以将一段代码作为参数传进方法中` 这句话？？
        // 例如下面的Comparator<Integer> cm = (x, y) -> Integer.compare(x, y); 它将Integer.compare(x, y)这段代码作为参数传进去了Comparator.compare,最终返回了一个匿名的函数，简化了以往创建内部类的复杂方式
        // 为什么传进去了方法Comparator.compare中而不是equals方法，需要跟参数个数，类型和返回值可以确定传进去的是Comparator中的哪个方法

    }

    @Test
    public void test3(){
        Consumer cm = (a) -> System.out.println(a);
        cm.accept("hi");
    }

    @Test
    public void test4(){
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        eval(list, n -> n>8);
    }

    public static void eval(List<Integer> list, Predicate<Integer> predicate) {
        for(Integer n: list) {
            if(predicate.test(n)) {
                System.out.println(n + " ");
            }
        }
    }


    /**
     * 方法引用 对象::方法名
     */
    @Test
    public void test5(){
        PrintStream out = System.out;
        Consumer<String> cm = x -> out.println(x);
        cm.accept("hi");

        Consumer<String> cm1 = out::println;
        cm1.accept("hello");

        Consumer<String> cm2 = System.out::println;
        cm1.accept("world");
    }

    /**
     * 方法引用 对象::方法名
     */
    @Test
    public void test6(){
        Student student = new Student("chen",1);
        Supplier<String> sup = () -> student.getName();
        System.out.println(sup.get());

        Supplier<String> sup2 = student::getName;
        System.out.println(sup2.get());
    }


    /**
     * 方法引用 类::静态方法名
     */
    @Test
    public void test7(){
        // 传统：静态内部类
        Comparator<Integer> com = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };

        // lambda方法
        Comparator<Integer> com1 = (x, y) -> Integer.compare(x, y);

        /**
         * 方法引用方式 类::静态方法
         * 这3种方法等价
         */
        Comparator<Integer> com2 = Integer::compare;
    }

    /**
     * 方法引用 类::实例方法名
     */
    @Test
    public void test8(){
        // BiPredicate也是java提供的函数式接口
        BiPredicate<String, String> bp1 = (x, y) -> x.equals(y);
        System.out.println(bp1.test("a", "a"));

        // 类::实例方法 的使用情况
        // 若lambda 参数列表中的第一个参数是 实例方法的调用者， 而第二个参数是实例方法的参数时，可以使用 类::实例方法名
        BiPredicate<String, String> bp2 = String::equals;
        System.out.println(bp2.test("a", "a"));
    }

    /**
     * 构造器引用 类::实例方法名
     */
    @Test
    public void test9(){
        Supplier<Student> sup = () -> new Student();
        Student student = sup.get();

        Supplier<Student> sup1 = Student::new;
        Student student1 = sup1.get();
    }

    /**
     * 构造器引用 类::new
     */
    @Test
    public void test10(){
        Function<String, Student> fun = (x) -> new Student(x);
        Student student = fun.apply("chenzeqin");
        System.out.println(student);

        Function<String, Student> fun1 = Student::new;
        Student student1 = fun1.apply("chen");
        System.out.println(student1);
    }

    /**
     * 数组引用 数组类型::new
     */
    @Test
    public void test11(){
        Function<Integer, String[]> fun = (x) -> new String[x];
        String[] str = fun.apply(10);
        System.out.println(str.length);

        Function<Integer, String[]> fun1 = String[]::new;
        String[] str1 = fun1.apply(20);
        System.out.println(str1.length);
    }

    @Test
    public void test12(){
        AFun aFun = x -> System.out.println(x);
        aFun.sysout("abc");
    }
}
