package com.chenzeqin.future;


import org.junit.Test;

import java.util.concurrent.*;

public class FutureTest {
    @Test
    public void test1() throws InterruptedException, ExecutionException {
        // 构建一个FutureTask对象，构造器需要传入一个Callable的实现对象。这里使用匿名内部类的方式去实现
        FutureTask ft = new FutureTask<>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("执行call方法");
                Thread.sleep(200000);
                return "success";
            }
        });

        // 开启一个线程去执行， 线程构造器这里也需要传入一个Runnable的对象，而FutureTask实现了Runnable的，所以直接放入ft
        Thread thread = new Thread(ft);
        thread.start();

        System.out.println("main线程干点其他事！");
        Thread.sleep(1000);

        // 通过ft去获取结果
        System.out.println("main线程获取执行的结果："+ ft.get());
    }

    @Test
    public void test2() throws InterruptedException, ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(5);

        Future<String> ft = executor.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("执行call方法");
                Thread.sleep(1000);
                return "success";
            }
        });

        System.out.println("main线程干点其他事！");
        Thread.sleep(1000);

        // 通过ft去获取结果
        System.out.println("main线程获取执行的结果："+ ft.get());

        new Thread(()->{
            try {
                Thread.sleep(15000);
                System.out.println("线程2获取执行的结果："+ ft.get());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }).start();
    }
}
