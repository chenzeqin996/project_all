package com.chenzeqin.stream;

import com.chenzeqin.java8.lambda.Student;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author chenzeqin
 * @menu
 * @date 2021-7-27
 */
public class StreamTest {
    List<Student> stuList = Arrays.asList(
            new Student("张三",18),
            new Student("李四",19),
            new Student("王五",20),
            new Student("赵六",21),
            new Student("田七",22)
    );

    @Test
    public void test(){
        Stream<Student> stream = stuList.stream().filter(x -> x.getAge()>=20);
        stream.forEach(System.out::println);
    }
}
