package com.chenzeqin.innerClass.anonymous;

public class anonymousTest {
    public static void main(String[] args) {
        // 匿名内部类的方式
        // 正常来说，我们接口是不能new出来的。但是这种方式的话创建的是一个匿名的内部类。代码块中会去重写接口的所有方法
        MyInterface myInterface = new MyInterface() {
            @Override
            public void display() {
                System.out.println("匿名内部类");
            }
        };
        myInterface.display();
    }
}


interface MyInterface{
    void display();
}