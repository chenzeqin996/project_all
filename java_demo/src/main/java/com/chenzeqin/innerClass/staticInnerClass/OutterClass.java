package com.chenzeqin.innerClass.staticInnerClass;

public class OutterClass {
    private String name;
    private void display(){
        System.out.println(name);
    }

    static class InnerClass{
        private String name;
        InnerClass(){
            name = "静态内部类";
        }
        public void display(){
            System.out.println(name);
        }
    }
}

class StaticInnerClassTest{
    public static void main(String[] args) {
        OutterClass.InnerClass innerClass = new OutterClass.InnerClass();
        innerClass.display();
    }
}
