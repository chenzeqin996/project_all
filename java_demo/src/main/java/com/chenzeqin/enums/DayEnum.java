package com.chenzeqin.enums;

public enum DayEnum {
    MONDAY,
    TUESDAY,
    WEDNESDAY;
}

class dayEnumTest{
    public static void main(String[] args) {
        DayEnum tuesday = DayEnum.valueOf("TUESDAY");
        System.out.println(tuesday);

        DayEnum monday = DayEnum.MONDAY;
        System.out.println(DayEnum.MONDAY == monday);
        System.out.println(DayEnum.MONDAY.equals(monday));
    }
}