package com.chenzeqin.map.concurrentHashMap;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author chenzeqin
 * date 2020/7/11
 */
public class CMapTest {
    public static void main(String[] args) {
//        Map m = new HashMap<>();
//        m.put(1,1);
//        ConcurrentHashMap map = new ConcurrentHashMap();
//        map.put("","");

        System.out.println(test());
    }

    private static int test(){
        int i = 10;
        try{
            return i;
        }finally {
            i = 20;
            return i;
        }
    }
}
