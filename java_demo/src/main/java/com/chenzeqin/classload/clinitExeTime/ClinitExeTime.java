package com.chenzeqin.classload.clinitExeTime;

import java.util.Random;

/**
 * 类加载过程-初始化阶段的主动调用时机测试，也就是clinit的执行时机
 */
public class ClinitExeTime {

    public static void main(String[] args) throws ClassNotFoundException {
        // 1.当创建一个类的实例时，比如使用new关键字，或者通过反射、克隆、反序列化。
        // Order order = new Order();

        // 2.当调用类的静态方法时，即当使用了字节码invokestatic指令
        // Order.staticMethod();

        // 3. 当使用类、接口的静态字段时(final修饰特殊考虑)，比如，使用getstatic或者putstatic指令。(对应访问变量赋值变量操作)
        // System.out.println(Order.num1);     // 会执行clinit方法
        // System.out.println(Order.num2);  // 不会执行clinit方法
        // System.out.println(Order.num3);  // 会执行clinit方法
        System.out.println(Order.numx);  // 会执行clinit方法

        // 4.当使用java.lang.reflect包中的方法反射类的方法时。比如:class.forName("com.atguigu.java.Test”)
        // Class clazz = Class.forName("com.chenzeqin.classload.clinitExeTime.Order");
    }

}
class Order {
    static {
        System.out.println("order执行静态代码块");
    }

    public static void staticMethod(){
        System.out.println("执行静态方法，触发clinit方法");
    }

    public static int num1 = 1;
    public static final Integer numx = 1;
    public static final int num2 = 1;
    public static final int num3 = new Random().nextInt(10);
}

interface InterA{
    default void test(){
        System.out.println("接口的default");
    }
}
