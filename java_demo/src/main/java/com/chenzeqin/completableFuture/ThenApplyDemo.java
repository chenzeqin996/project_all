package com.chenzeqin.completableFuture;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class ThenApplyDemo {
    public static void main(String[] args) throws Exception {
        CommonUtils.printThreadLog("main start");

        // CompletableFuture<String> readFileFuture = CompletableFuture.supplyAsync(() -> {
        //     CommonUtils.printThreadLog("读取filter_words文件");
        //     String filterWordsContent = CommonUtils.readFile("filter_words.txt");
        //     return filterWordsContent;
        // });
        //
        // // 调用thenApply方法，传入一个function函数式接口，处理结果。参数就是supplyAsync处理的返回值
        // CompletableFuture<String[]> filterWordsFuture = readFileFuture.thenApply((content) -> {
        //     CommonUtils.printThreadLog("文件内容转换成敏感词数组");
        //     String[] filterWords = content.split(",");
        //     return filterWords;
        // });

        CompletableFuture<String[]> readFileFuture = CompletableFuture.supplyAsync(() -> {
            CommonUtils.printThreadLog("读取filter_words文件");
            String filterWordsContent = CommonUtils.readFile("filter_words.txt");
            return filterWordsContent;
        }).thenApply((content) -> {
            CommonUtils.printThreadLog("文件内容转换成敏感词数组");
            String[] filterWords = content.split(",");
            return filterWords;
        });

        CommonUtils.printThreadLog("main continue");

        String[] filterWords = readFileFuture.get();
        CommonUtils.printThreadLog("filterWords = " + Arrays.toString(filterWords));
        CommonUtils.printThreadLog("main end");
    }
}