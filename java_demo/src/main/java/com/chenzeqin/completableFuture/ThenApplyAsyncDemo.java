package com.chenzeqin.completableFuture;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

public class ThenApplyAsyncDemo {
    public static void main(String[] args) throws Exception {
        CommonUtils.printThreadLog("main start");

        CompletableFuture<String[]> filterWordFuture = CompletableFuture.supplyAsync(() -> {
            /*
            CommonUtils.printThreadLog("读取filter_words文件");
            String filterWordsContent =
            CommonUtils.readFile("filter_words.txt");
            return filterWordsContent;
            */
            // 此时，立即返回结果
            return "尼玛, NB, tmd";
        }).thenApply((content) -> {
            /**
             * 一般而言，thenApply任务的执行和supplyAsync()任务执行可以使用
             同一线程执行
             * 如果supplyAsync()任务立即返回结果，则thenApply的任务在主线程
             中执行
             */
            CommonUtils.printThreadLog("把内容转换成敏感词数组");
            String[] filterWords = content.split(",");
            return filterWords;
        });


        CommonUtils.printThreadLog("main continue");
        String[] filterWords = filterWordFuture.get();
        CommonUtils.printThreadLog("filterWords = " + Arrays.toString(filterWords));
        CommonUtils.printThreadLog("main end");
    }
}
