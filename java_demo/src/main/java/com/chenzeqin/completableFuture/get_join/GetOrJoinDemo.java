package com.chenzeqin.completableFuture.get_join;


import com.chenzeqin.completableFuture.CommonUtils;

import java.util.concurrent.CompletableFuture;

public class GetOrJoinDemo {
    public static void main(String[] args) {
        // get or join
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            return "hello";
        });

        String ret = future.join();
        CommonUtils.printThreadLog("ret = " + ret);

        // get()的方式，需要处理异常
        /*try {
            String ret = future.get();
            CommonUtils.printThreadLog("ret = " + ret);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }*/
    }
}
