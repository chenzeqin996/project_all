package com.chenzeqin.completableFuture;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;

public class CommonUtils {
    // 从文件中读取内容
    public static String readFile(String pathToFile) {
        try {
            // return Files.readString(Paths.get(pathToFile));
            return "这是CompletableFuture测试字符串TMD";
        } catch(Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    // 休眠毫秒
    public static void sleepMillis(long millis) {
        try {
            TimeUnit.MILLISECONDS.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // 休眠秒
    public static void sleepSecond(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // 打印信息，输出结果：时间 | 线程ID | 线程名 | message
    public static void printThreadLog(String message) {
        String result = new StringJoiner(" | ")
                .add(String.valueOf(System.currentTimeMillis()))
                .add(String.format("%2d",Thread.currentThread().getId()))
                .add(String.valueOf(Thread.currentThread().getName()))
                .add(message)
                .toString();
        System.out.println(result);
    }
}
