package com.chenzeqin.collections.sort;

/**
 * @author chenzeqin
 * @menu
 * @date 2020/12/8
 */
public class Score1 implements Comparable<Score1>{
    private String name;
    private Double score;

    public Score1(String name, Double score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Score1{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }


    public int compareTo(Score1 o) {
        return (int) (this.getScore() - o.getScore());
    }

}
