package com.chenzeqin.collections.sort;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author chenzeqin
 * @menu
 * @date 2020/12/8
 */
public class Sort {
    public static void main(String[] args) {


        // 例1
        List<Integer> ilist = Arrays.asList(2,1,4,5,6,0);
        System.out.println("排序前：");
        printList(ilist);
        // 默认升序
        Collections.sort(ilist);
        System.out.println("排序后：");
        printList(ilist);

        // 例2
        List<Score1> slist = Arrays.asList(new Score1("zhangsan",60d)
                ,new Score1("lisi",55d)
                ,new Score1("wangwu",99.5) );
        System.out.println("排序前：");
        printList(slist);
        Collections.sort(slist);
        System.out.println("排序后：");
        printList(slist);

        // 例3
        List<Score2> slist2 = Arrays.asList(new Score2("zhangsan",60d)
                ,new Score2("lisi",55d)
                ,new Score2("wangwu",99.5) );
        System.out.println("排序前：");
        printList(slist2);
        Collections.sort(slist2, new Comparator<Score2>() {
            public int compare(Score2 o1, Score2 o2) {
                return (int) (o1.getScore()-o2.getScore());
            }
        });
        System.out.println("排序后：");
        printList(slist2);

        // 例4
        List<Score2> slist3 = Arrays.asList(new Score2("zhangsan",60d)
                ,new Score2("lisi",55d)
                ,new Score2("wangwu",99.5) );
        System.out.println("slist3排序前：");
        printList(slist3);
        Comparator c = new Comparator<Score2>() {
            public int compare(Score2 o1, Score2 o2) {
                return (int) (o1.getScore()-o2.getScore());
            }
        };
        Collections.sort(slist3, c);
        System.out.println("slist3排序后：");
        Collections.sort(slist3, c.reversed());
        System.out.println("slist3排序后reversed：");
        printList(slist3);

        Integer str1 = new Integer(1);
        Integer str2 = new Integer(2);
        System.out.println(str1.compareTo(str2));;
    }

    private static void printList(List list){
        for (Object o : list) {
            System.out.print(o.toString()+", ");
        }
        System.out.println();
    }
}
