package com.chenzeqin.designPatterns.singleton;

import java.lang.reflect.Constructor;

/**
 * 静态内部类获取单例
 * @author: chenzeqin
 * @date: 2022/4/16
 */
public class InnerClassSignleton {
    // 1.构造方法私有化，防止new的方式创建对象
    private InnerClassSignleton(){
        if(InnerClassHolder.INS != null){
            throw new RuntimeException("单例模式不允许多个实例！");
        }
    }

    // 2.采用静态内部类 的静态成员变量，去创建单例对象
    private static class InnerClassHolder{
        private static InnerClassSignleton INS = new InnerClassSignleton();
    }

    // 3.提供一个静态方法，作为外部获取单例对象的唯一入口
    public static InnerClassSignleton getInstance(){
        return InnerClassHolder.INS;
    }

    public static void main(String[] args) throws Exception{
        new Thread(() ->{
            InnerClassSignleton instance = InnerClassSignleton.getInstance();
            System.out.println(instance);
        }).start();
        new Thread(() ->{
            InnerClassSignleton instance = InnerClassSignleton.getInstance();
            System.out.println(instance);
        }).start();

        // 反射攻击
        Constructor<InnerClassSignleton> constructor = InnerClassSignleton.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        InnerClassSignleton instance3 = constructor.newInstance();
        System.out.println("反射攻击输出："+instance3);
    }
}
