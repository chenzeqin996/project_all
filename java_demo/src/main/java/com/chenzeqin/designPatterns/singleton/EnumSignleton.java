package com.chenzeqin.designPatterns.singleton;

import java.lang.reflect.Constructor;

public enum EnumSignleton {
    INSTANCE;

}

class EnumSignletonTest{
    public static void main(String[] args) throws Exception {
        EnumSignleton ins1 = EnumSignleton.INSTANCE;
        EnumSignleton ins2 = EnumSignleton.INSTANCE;
        System.out.println(ins1 == ins2);

        // 反射攻击
        // Constructor<EnumSignleton> constructor = EnumSignleton.class.getDeclaredConstructor();
        Constructor<EnumSignleton> constructor = EnumSignleton.class.getDeclaredConstructor(String.class, int.class);
        constructor.setAccessible(true);
        EnumSignleton ins3 = constructor.newInstance();
        System.out.println("反射攻击输出："+ins3);

    }
}
