package com.chenzeqin.designPatterns.singleton;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * 单例模式-懒汉加载：延迟加载，只有在真正使用对象的时候才实例化
 * @author chenzeqin
 * date 2020/8/10
 */
public class LazySingleton implements Serializable {
    private volatile static LazySingleton lazySingleton;
    // 将默认的构造方法 设置为private，那么外部将无法通过new来创建对象了
    private LazySingleton() throws Exception {
        if(lazySingleton != null){
            throw new Exception("不允许创建对象");
        }
    }

    // 想法1：这种方法看起来没啥问题，但是在并发的情况下，还是都new了一个新的实例出来（所以需要考虑并发问题）
    public static LazySingleton getInstance1() throws Exception {
        if(lazySingleton==null){
            lazySingleton = new LazySingleton();
        }
        return lazySingleton;
    }

    // 想法2：既然有并发问题，那我们直接在这个静态方法加锁不就行了吗。这样确实没问题；但是整个方法都被锁了，相当于我每获取对象都会加锁一次，性能不好
    public synchronized static LazySingleton getInstance2() throws Exception {
        if(lazySingleton==null){
            lazySingleton = new LazySingleton();
        }
        return lazySingleton;
    }

    // 想法3：既然锁加载方法上性能不好，那我们只需要在new对象的时候加锁就行了。就像下面一样
    // 但是这样写还是有问题的，比如并发情况下有两个线程刚好都执行完lazySingleton==null，这时候都会进行申请加锁，
    // 线程A获取到锁之后new了一个对象，线程B等待A释放锁之后，也会去new一个对象。所以这种写法有可能有线程安全问题
    public synchronized static LazySingleton getInstance3() throws Exception {
        if(lazySingleton==null){
            synchronized (LazySingleton.class){
                lazySingleton = new LazySingleton();
            }
        }
        return lazySingleton;
    }

    // 想法4：在synchronized获取到锁之后，在判断lazySingleton==null，也就是double check lock;这种就是最终的懒汉模式的解决方法
    // 但是在字节码层面，可能会有执行重排，所以需要加上volatile防止指令重排
    public static LazySingleton getInstance() throws Exception {
        if(lazySingleton==null){
            synchronized (LazySingleton.class){
                if(lazySingleton==null){
                    lazySingleton = new LazySingleton();
                    // 在字节码层面，new命令会执行有以下几个步骤
                    // 1、分配空间
                    // 2、初始化
                    // 3、引用赋值
                    // 在多核CPU，会有指令重排序的情况，即2，3步骤执行顺序有可能相反，执行顺序有1，3，2。
                    // 所以在多线程情况下，线程2拿到的有可能是在线程1，3执行完毕，而2未执行（未进行初始化）的数据。
                    // 所以加上volatile可以防止指令的重排序
                }
            }
        }
        return lazySingleton;
    }

}

class LazySingTest{
    public static void main(String[] args) throws InterruptedException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        // 反射攻击
        Constructor<LazySingleton> con = LazySingleton.class.getDeclaredConstructor();
        con.setAccessible(true);
        LazySingleton obj = con.newInstance();
        System.out.println("反射对象"+obj);

        new Thread(()->{
            LazySingleton instance = null;
            try {
                instance = LazySingleton.getInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            System.out.println(instance);
        }).start();

        new Thread(()->{
            LazySingleton instance = null;
            try {
                instance = LazySingleton.getInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            System.out.println(instance);
        }).start();
        Thread.sleep(1000l);


    }
}
