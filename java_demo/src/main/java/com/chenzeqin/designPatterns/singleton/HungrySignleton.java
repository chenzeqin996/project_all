package com.chenzeqin.designPatterns.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * 饿汉模式
 * @author: chenzeqin
 * @date: 2022/4/16
 */
public class HungrySignleton{
    // 2.通过静态变量，进行对象的创建
    private static HungrySignleton instance = new HungrySignleton();

    // 1.构造方法私有化，防止new的方式创建对象
    private HungrySignleton() {
        // 加上判断可以防止反射工具
        /*if(instance!=null){
            throw new RuntimeException("单例模式不允许多个实例！");
        }*/
    }

    // 3.提供一个静态方法，作为外部获取单例对象的唯一入口
    public static HungrySignleton getInstance(){
        return instance;
    }


    public static void main(String[] args) throws Exception{
        HungrySignleton instance = HungrySignleton.getInstance();
        HungrySignleton instance2 = HungrySignleton.getInstance();
        System.out.println(instance);
        System.out.println(instance2);

        // 反射攻击
        // Constructor<HungrySignleton> constructor = HungrySignleton.class.getDeclaredConstructor();
        // constructor.setAccessible(true);
        // HungrySignleton instance3 = constructor.newInstance();
        // System.out.println(instance3);
    }
}
