package com.chenzeqin.designPatterns.singleton;

import java.io.*;

/**
 * 测试反序列化对单例的破坏
 * @author: chenzeqin
 * @date: 2022/4/16
 */
public class SerializableTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        HungrySignleton1 instance = HungrySignleton1.getInstance();
        System.out.println(instance);
        // 将实例序列化到本地磁盘
        // ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("testSerializable"));
        // oos.writeObject(instance);
        // oos.close();
        // 读取磁盘反序列化获取实例
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("testSerializable"));
        HungrySignleton1 serialIns = (HungrySignleton1) ois.readObject();
        System.out.println(serialIns);
    }
}

class HungrySignleton1 implements Serializable {
    private static final long serialVersionUID = 222L;

    private static HungrySignleton1 INS = new HungrySignleton1();

    HungrySignleton1() {
    }

    public static HungrySignleton1 getInstance(){
        return INS;
    }

    Object readResolve() throws ObjectStreamException{
        return INS;
    }

    // private int aa = 1;
}
