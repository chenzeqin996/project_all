package com.chenzeqin.designPatterns.factory.method;

/**
 * @author: chenzeqin
 * @date: 2022/4/17
 */
public class TeslaCar implements Car {
    @Override
    public void name() {
        System.out.println("特斯拉汽车");
    }
}
