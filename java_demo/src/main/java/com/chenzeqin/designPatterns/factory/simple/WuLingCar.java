package com.chenzeqin.designPatterns.factory.simple;

/**
 * @author: chenzeqin
 * @date: 2022/4/17
 */
public class WuLingCar implements Car{
    @Override
    public void name() {
        System.out.println("五菱宏光汽车");
    }
}
