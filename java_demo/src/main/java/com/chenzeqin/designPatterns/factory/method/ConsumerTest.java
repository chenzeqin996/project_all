package com.chenzeqin.designPatterns.factory.method;

import com.chenzeqin.designPatterns.factory.simple.TeslaCar;
import com.chenzeqin.designPatterns.factory.simple.WuLingCar;

/**
 * 测试简单工厂模式
 * @author: chenzeqin
 * @date: 2022/4/17
 */
public class ConsumerTest {

    public static void main(String[] args) {
        Car car1 = new WuLingCarFactory().getCar();
        Car car2 = new TeslaCarFactory().getCar();
        car1.name();
        car2.name();
    }
}
