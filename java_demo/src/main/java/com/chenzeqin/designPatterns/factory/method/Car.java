package com.chenzeqin.designPatterns.factory.method;

public interface Car {
    void name();
}
