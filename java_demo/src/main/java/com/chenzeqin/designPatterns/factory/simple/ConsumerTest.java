package com.chenzeqin.designPatterns.factory.simple;

/**
 * 测试简单工厂模式
 * @author: chenzeqin
 * @date: 2022/4/17
 */
public class ConsumerTest {

    public static void main(String[] args) {
        // 不使用任何模式，消费者将直接new创建对象
        Car car1 = new WuLingCar();
        Car car2 = new TeslaCar();
        car1.name();
        car2.name();

        // 通过简单工厂模式获取Car对象
        Car car11 = CarFactory.getCar("WuLing");
        Car car22 = CarFactory.getCar("Tesla");
        car11.name();
        car22.name();
    }
}
