package com.chenzeqin.designPatterns.factory.abstractFa;

/**
 * 定义路由接口，以及实现类
 * @author: chenzeqin
 * @date: 2022/4/17
 */
public interface RouteI {
    void startWifi();
}

class XiaomiRoute implements RouteI{
    @Override
    public void startWifi() {
        System.out.println("开启小米wifi");
    }
}

class HuaweiRoute implements RouteI{
    @Override
    public void startWifi() {
        System.out.println("开启华为wifi");
    }
}