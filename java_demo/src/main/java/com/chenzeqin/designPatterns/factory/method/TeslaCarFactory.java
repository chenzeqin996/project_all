package com.chenzeqin.designPatterns.factory.method;

/**
 * @author: chenzeqin
 * @date: 2022/4/17
 */
public class TeslaCarFactory implements CarFactory{
    @Override
    public Car getCar() {
        return new TeslaCar();
    }
}
