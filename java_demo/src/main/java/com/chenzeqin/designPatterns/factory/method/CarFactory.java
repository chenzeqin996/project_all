package com.chenzeqin.designPatterns.factory.method;

public interface CarFactory {
    Car getCar();
}
