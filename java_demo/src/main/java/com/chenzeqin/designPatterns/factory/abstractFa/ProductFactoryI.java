package com.chenzeqin.designPatterns.factory.abstractFa;

/**
 * 抽象工厂模式
 * @author: chenzeqin
 * @date: 2022/4/17
 */
public interface ProductFactoryI {
    PhoneI getPhone();
    RouteI getRoute();
}

class XiaoFactory implements ProductFactoryI{
    @Override
    public PhoneI getPhone() {
        return new XiaomiPhone();
    }

    @Override
    public RouteI getRoute() {
        return new XiaomiRoute();
    }
}

class HuaweiFactory implements ProductFactoryI{
    @Override
    public PhoneI getPhone() {
        return new HuaweiPhone();
    }

    @Override
    public RouteI getRoute() {
        return new HuaweiRoute();
    }
}