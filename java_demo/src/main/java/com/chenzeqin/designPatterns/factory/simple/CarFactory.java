package com.chenzeqin.designPatterns.factory.simple;

/**
 * 简单工厂模式，获取car实例
 * @author: chenzeqin
 * @date: 2022/4/17
 */
public class CarFactory {
    public static Car getCar(String name){
        if("WuLing".equals(name)){
            return new WuLingCar();
        }else if ("Tesla".equals(name)){
            return new TeslaCar();
        }
        return null;
    }
}
