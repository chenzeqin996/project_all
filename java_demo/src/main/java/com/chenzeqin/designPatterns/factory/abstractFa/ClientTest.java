package com.chenzeqin.designPatterns.factory.abstractFa;

/**
 * 测试抽象工厂
 * @author: chenzeqin
 * @date: 2022/4/17
 */
public class ClientTest {
    public static void main(String[] args) {
        ProductFactoryI factory = new XiaoFactory();
        PhoneI phone = factory.getPhone();
        RouteI route = factory.getRoute();
        phone.call();
        route.startWifi();
    }
}
