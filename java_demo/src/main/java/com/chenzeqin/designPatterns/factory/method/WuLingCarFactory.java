package com.chenzeqin.designPatterns.factory.method;

/**
 * @author: chenzeqin
 * @date: 2022/4/17
 */
public class WuLingCarFactory implements CarFactory{
    @Override
    public Car getCar() {
        return new WuLingCar();
    }
}
