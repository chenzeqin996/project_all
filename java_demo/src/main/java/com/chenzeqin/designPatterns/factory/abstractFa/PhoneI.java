package com.chenzeqin.designPatterns.factory.abstractFa;

/**
 * 手机产品接口，以及实现类
 * @author: chenzeqin
 * @date: 2022/4/17
 */
public interface PhoneI {
    void call();
}

class XiaomiPhone implements PhoneI{
    @Override
    public void call() {
        System.out.println("小米手机打电话");
    }
}

class HuaweiPhone implements PhoneI{
    @Override
    public void call() {
        System.out.println("华为手机打电话");
    }
}