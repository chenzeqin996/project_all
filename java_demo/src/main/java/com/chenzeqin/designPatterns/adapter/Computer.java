package com.chenzeqin.designPatterns.adapter;

/**
 * 电脑
 */
public class Computer {
    public void net(NetToUsbI netToUsbI){
        netToUsbI.handleRequest();
    }
}
