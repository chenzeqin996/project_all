package com.chenzeqin.designPatterns.adapter;

/**
 * 适配器模式
 * @author: chenzeqin
 * @date: 2022/4/25
 */
public class Client {
    public static void main(String[] args) {
        System.out.println("第一种适配器,使用继承的方式（类适配器），单继承");
        Adapter adapter = new Adapter();
        Computer computer = new Computer();
        computer.net(adapter);

        System.out.println("第二种适配器，组合（对象适配器），常用");
        Adaptee adaptee = new Adaptee();            // 网线
        Adapter2 adapter2 = new Adapter2(adaptee);  // 适配器，接网线
        computer.net(adapter2);                     // 笔记本连接适配器，实现上网
    }
}
