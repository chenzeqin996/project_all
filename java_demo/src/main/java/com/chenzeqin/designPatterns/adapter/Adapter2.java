package com.chenzeqin.designPatterns.adapter;

/**
 * 类适配器(即转接器)，对象适配器模式，使用的是组合模式
 */
public class Adapter2 implements NetToUsbI{
    private Adaptee adaptee;

    Adapter2(Adaptee adaptee){
        this.adaptee = adaptee;
    }
    @Override
    public void handleRequest() {
        adaptee.connectNetWork();
    }
}
