package com.chenzeqin.designPatterns.adapter;

/**
 * 要被适配的类，即网线
 */
public class Adaptee {
    public void connectNetWork(){
        System.out.println("连接网线，上网！");
    }
}
