package com.chenzeqin.designPatterns.adapter;

/**
 * 网线接口转usb接口
 */
public interface NetToUsbI {
    void handleRequest();
}
