package com.chenzeqin.designPatterns.adapter;

/**
 * 类适配器(即转接器)，使用的是继承实现的
 */
public class Adapter extends Adaptee implements NetToUsbI{
    @Override
    public void handleRequest() {
        super.connectNetWork();
    }
}
