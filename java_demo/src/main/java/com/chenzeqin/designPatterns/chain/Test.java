package com.chenzeqin.designPatterns.chain;

/**
 * 责任链模式
 * 参考文档 https://mp.weixin.qq.com/s/VizKKaHoCxvmXL0EPntKZA
 * @author chenzeqin
 * @date 2023/3/13
 */
public class Test {

    public static void main(String[] args) {
        Handler ahandler = new AHandlerImpl();
        Handler bhandler = new BHandlerImpl();
        ahandler.setNextHandler(bhandler);

        System.out.println(ahandler.handleRequest("A"));
        System.out.println(ahandler.handleRequest("B"));
        System.out.println(ahandler.handleRequest("C"));
    }
}
