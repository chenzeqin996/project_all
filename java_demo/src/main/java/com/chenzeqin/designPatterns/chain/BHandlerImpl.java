package com.chenzeqin.designPatterns.chain;

/**
 * @author chenzeqin
 * @date 2023/3/13
 */
public class BHandlerImpl extends Handler{

    @Override
    int handleRequest(String obj) {
        System.out.println("执行b处理器方法");
        if("B".equals(obj)){
            return 2;
        }else {
            return -1;
        }
    }
}
