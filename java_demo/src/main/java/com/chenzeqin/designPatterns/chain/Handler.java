package com.chenzeqin.designPatterns.chain;

/**
 * 抽象方法：主要有2核心字段
 * 1.定义下一个要处理的处理器
 * 2.具体处理的方法
 */
public abstract class Handler {
    // 下个处理器
    Handler nextHandler;
    // 处理方法
    abstract int handleRequest(String obj);

    public void setNextHandler(Handler nextHandler){
        this.nextHandler = nextHandler;
    }
}
