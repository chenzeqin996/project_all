package com.chenzeqin.designPatterns.chain;

/**
 * @author chenzeqin
 * @date 2023/3/13
 */
public class AHandlerImpl extends Handler{

    @Override
    int handleRequest(String obj) {
        System.out.println("执行a处理器方法");
        if("A".equals(obj)){
            return 1;
        }
        return nextHandler.handleRequest(obj);
    }
}
