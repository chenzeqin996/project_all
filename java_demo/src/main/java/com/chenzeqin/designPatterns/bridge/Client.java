package com.chenzeqin.designPatterns.bridge;

/**
 * @author: chenzeqin
 * @date: 2022/4/25
 */
public class Client {
    public static void main(String[] args) {
        // 华为笔记本
        Computer computer1 = new DesktopComputer(new HuaWeiBrand());
        computer1.info();
        // 小米台式电脑
        Computer computer2 = new DesktopComputer(new XiaoMiBrand());
        computer2.info();
    }
}
