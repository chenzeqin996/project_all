package com.chenzeqin.designPatterns.bridge;

// 品牌接口
public interface Brand {
    void info();
}

class HuaWeiBrand implements Brand{
    @Override
    public void info() {
        System.out.print("华为品牌");
    }
}

class XiaoMiBrand implements Brand{
    @Override
    public void info() {
        System.out.print("小米品牌");
    }
}