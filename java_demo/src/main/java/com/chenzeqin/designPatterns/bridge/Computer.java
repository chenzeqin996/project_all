package com.chenzeqin.designPatterns.bridge;

// 电脑接口
public abstract class Computer {
    // 组合方式，电脑自带品牌属性，protected属性同包子类可以使用
    protected Brand brand;

    Computer(Brand brand){
        this.brand = brand;
    }

    abstract void info();
}

class LaptopComputer extends Computer {
    LaptopComputer(Brand brand) {
        super(brand);
    }

    @Override
    public void info() {
        brand.info();
        System.out.println("笔记本电脑");
    }
}

class DesktopComputer extends Computer{
    DesktopComputer(Brand brand) {
        super(brand);
    }

    @Override
    public void info() {
        brand.info();
        System.out.println("台式电脑");
    }
}