package com.chenzeqin.designPatterns.observerModel.demo01.impl;

import com.chenzeqin.designPatterns.observerModel.demo01.Observer;
import com.chenzeqin.designPatterns.observerModel.demo01.Subject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenzeqin
 * @menu 发送朋友圈主题
 * @date 2021-6-4
 */
public class SendPYQSubject implements Subject {
    // 订阅者容器
    private List<Observer> observers = new ArrayList<Observer>();

    @Override
    public void attach(Observer observer) {
        // 添加订阅关系
        observers.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        // 移除订阅关系
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(String message) {
        // 通知订阅者们
        if(observers!=null && observers.size()>0){
            for (Observer observer : observers) {
                // 这里相当于我发了一条朋友圈，然后加了好友的（即订阅者们），我们都通知他们一下
                observer.update(message);
            }
        }
    }
}
