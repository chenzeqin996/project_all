package com.chenzeqin.designPatterns.observerModel.demo01.impl;

import com.chenzeqin.designPatterns.observerModel.demo01.Observer;

/**
 * @author chenzeqin
 * @menu 订阅者1
 * @date 2021-6-4
 */
public class FriendOneObserver implements Observer {
    @Override
    public void update(String message) {
        // 处理业务....
        System.out.println(this.getClass().getSimpleName()+" 知道了你发了朋友圈："+message);
    }
}
