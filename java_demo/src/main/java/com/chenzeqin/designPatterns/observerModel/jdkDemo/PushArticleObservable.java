package com.chenzeqin.designPatterns.observerModel.jdkDemo;

import java.util.Observable;

/**
 * @author chenzeqin
 * @menu 发表文章主题
 * @date 2021-6-4
 */
public class PushArticleObservable extends Observable {

    private String article;

    // 发表文章
    public void publish(String article){
        this.article = article;
        // 改变状态，表示有动态
        this.setChanged();
        // 通知订阅者们
        this.notifyObservers();
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }
}
