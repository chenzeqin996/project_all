package com.chenzeqin.designPatterns.observerModel.demo01;

/**
 * 订阅者
 * @author chenzeqin
 * @date 2021-6-4
 * @return
 */
public interface Observer {

    // 处理业务逻辑
    void update(String message);
}
