package com.chenzeqin.designPatterns.observerModel.demo01;

import com.chenzeqin.designPatterns.observerModel.demo01.impl.FriendOneObserver;
import com.chenzeqin.designPatterns.observerModel.demo01.impl.FriendTwoObserver;
import com.chenzeqin.designPatterns.observerModel.demo01.impl.SendPYQSubject;

/**
 * @author chenzeqin
 * @menu
 * @date 2021-6-4
 */
public class Main {

    /**
     * 测试观察者模式
     * @param args
     */
    public static void main(String[] args) {
        FriendOneObserver f1 = new FriendOneObserver();
        FriendTwoObserver f2 = new FriendTwoObserver();

        Subject sub = new SendPYQSubject();
        // 添加好友（添加订阅）
        sub.attach(f1);
        sub.attach(f2);
        // 发了朋友圈通知朋友们
        sub.notifyObservers("wo：今天天气真好！");

        // 移除好友（取消订阅）
        sub.detach(f2);
        sub.notifyObservers("wo：第二条朋友圈！");
    }
}
