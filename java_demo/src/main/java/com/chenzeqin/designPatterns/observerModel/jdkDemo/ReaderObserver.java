package com.chenzeqin.designPatterns.observerModel.jdkDemo;

import java.util.Observable;
import java.util.Observer;

/**
 * @author chenzeqin
 * @menu 读者
 * @date 2021-6-4
 */
public class ReaderObserver implements Observer {
    // 读者名称
    private String readerName;
    // 文章
    private String article;

    public ReaderObserver() {
    }

    public ReaderObserver(String readerName) {
        this.readerName = readerName;
    }

    @Override
    public void update(Observable o, Object arg) {
        PushArticleObservable p = (PushArticleObservable) o;
        System.out.println("我是读者"+this.getReaderName()+",收到来自"+p.getClass().getSimpleName()+"的新文章："+ p.getArticle());
    }

    public String getReaderName() {
        return readerName;
    }

    public void setReaderName(String readerName) {
        this.readerName = readerName;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }
}
