package com.chenzeqin.designPatterns.observerModel.jdkDemo;

/**
 * @author chenzeqin
 * @menu
 * @date 2021-6-4
 */
public class Main {
    public static void main(String[] args) {
        // 创建观察目标
        PushArticleObservable p = new PushArticleObservable();
        // 添加订阅者
        p.addObserver(new ReaderObserver("小明"));
        p.addObserver(new ReaderObserver("小红"));
        p.addObserver(new ReaderObserver("小白"));

        // 发表新文章
        p.publish("《十万个为什么》");
    }
}
