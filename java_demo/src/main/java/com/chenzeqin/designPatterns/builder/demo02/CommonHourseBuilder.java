package com.chenzeqin.designPatterns.builder.demo02;

/**
 * @author chenzeqin
 * @menu 房屋具体的建造者（具体怎么建造房子）
 * @date 2022-4-24
 */
public class CommonHourseBuilder extends HourseBuilder {
    private Hourse hourse = new Hourse();

    @Override
    HourseBuilder buildBasic() {
        hourse.setBasic("地基5米");
        System.out.println("普通房屋 - 打地基5米");
        return this;
    }

    @Override
    HourseBuilder buildWalls() {
        hourse.setWalls("墙壁100平");
        System.out.println("普通房屋 - 砌墙100平");
        return this;
    }

    @Override
    HourseBuilder buildRoofed() {
        hourse.setRoofed("瓦片屋顶");
        System.out.println("普通房屋 - 盖瓦片屋顶");
        return this;
    }

    @Override
    Hourse build() {
        return hourse;
    }
}
