package com.chenzeqin.designPatterns.builder.demo03;

/**
 * @author chenzeqin
 * @menu 测试客户端
 * @date 2022-4-24
 */
public class Client {

    /**
     * 建造者模式的最佳实现，即省去了指挥者，又省去了抽象建造者；而在产品类内部维护一个静态内部类去实现具体的建造
     */
    public static void main(String[] args) {
        Hourse hourse = Hourse.builder().setBasic("5米地基").setWalls("10平墙壁").setRoofed("瓦片屋顶").build();
        System.out.println(hourse);

        // 实际就是lombok @Builder的具体实现方式
        Hourse2 hourse2 = Hourse2.builder().basic("5米地基").walls("10平墙壁").build();
        System.out.println(hourse2);
    }
}
