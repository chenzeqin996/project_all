package com.chenzeqin.designPatterns.builder.demo01;

/**
 * @author chenzeqin
 * @menu 测试客户端
 * @date 2022-4-24
 */
public class Client {

    public static void main(String[] args) {
        HourseDirector hourseDirector = new HourseDirector();
        Hourse hourse = hourseDirector.buildHourse(new CommonHourseBuilder());
        System.out.println(hourse);
    }
}
