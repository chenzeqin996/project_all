package com.chenzeqin.designPatterns.builder.demo03;

import lombok.Data;

/**
 * @author chenzeqin
 * @menu 房屋-产品
 * @date 2022-4-24
 */
@Data
public class Hourse {
    // 基地
    private String basic;
    // 墙壁
    private String walls;
    // 屋顶
    private String roofed;

    private Hourse(String basic, String walls, String roofed){
        this.basic = basic;
        this.walls = walls;
        this.roofed = roofed;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder {
        private String basic;   // 基地
        private String walls;   // 墙壁
        private String roofed;

        public Builder setBasic(String basic) {
            this.basic = basic;
            return this;
        }

        public Builder setWalls(String walls) {
            this.walls = walls;
            return this;
        }

        public Builder setRoofed(String roofed) {
            this.roofed = roofed;
            return this;
        }

        public Hourse build() {
            Hourse hourse = new Hourse(this.basic, this.walls, this.roofed);
            return hourse;
        }
    }
}
