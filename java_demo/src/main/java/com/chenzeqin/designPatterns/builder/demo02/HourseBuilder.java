package com.chenzeqin.designPatterns.builder.demo02;

/**
 * @author chenzeqin
 * @menu 房屋抽象建造者
 * @date 2022-4-24
 */
public abstract class HourseBuilder {
    abstract HourseBuilder buildBasic(); // 打地基
    abstract HourseBuilder buildWalls(); // 砌墙
    abstract HourseBuilder buildRoofed();// 建屋顶

    abstract Hourse build();    // 返回建造的房屋
}
