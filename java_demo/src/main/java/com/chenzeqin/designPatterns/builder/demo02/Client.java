package com.chenzeqin.designPatterns.builder.demo02;

/**
 * @author chenzeqin
 * @menu 测试客户端
 * @date 2022-4-24
 */
public class Client {

    /**
     * 该建造者模式相比于demo,少了Director指挥者这个角色。由客户端来充当指挥者；房屋怎么建造由我们客户端自己指定；这就是链式编程
     */
    public static void main(String[] args) {
        HourseBuilder hourseBuilder = new CommonHourseBuilder();
        Hourse hourse = hourseBuilder.buildBasic().buildRoofed().buildWalls().build();
        System.out.println(hourse);
    }
}
