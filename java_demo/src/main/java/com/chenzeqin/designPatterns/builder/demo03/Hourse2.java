package com.chenzeqin.designPatterns.builder.demo03;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Hourse2 {
    // 基地
    private String basic;
    // 墙壁
    private String walls;
    // 屋顶
    private String roofed;

}
