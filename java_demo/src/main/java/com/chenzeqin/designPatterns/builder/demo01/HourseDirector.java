package com.chenzeqin.designPatterns.builder.demo01;

/**
 * @author chenzeqin
 * @menu 建房子的指挥者
 * @date 2022-4-24
 */
public class HourseDirector {

    public Hourse buildHourse(HourseBuilder hourseBuilder){
        hourseBuilder.buildBasic();
        hourseBuilder.buildWalls();
        hourseBuilder.buildRoofed();
        return hourseBuilder.build();
    }
}
