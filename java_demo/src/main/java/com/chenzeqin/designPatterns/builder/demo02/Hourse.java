package com.chenzeqin.designPatterns.builder.demo02;

import lombok.Data;

/**
 * @author chenzeqin
 * @menu 房屋-产品
 * @date 2022-4-24
 */
@Data
public class Hourse {
    // 基地
    private String basic;
    // 墙壁
    private String walls;
    // 屋顶
    private String roofed;
}
