package com.chenzeqin.designPatterns.templateMethod;

/***
 * 模版方法
 */
public abstract class AbstractClazz {
    // 抽象方法，由子类来实现
    abstract void templateMethod();

    public void operation(){
        // 定义方法的骨架，templateMethod由具体的子类来实现
        System.out.println(" 第一步");
        // ...
        System.out.println(" 第二步");
        // ...
        System.out.println(" 第三步");
        templateMethod();

    }
}
