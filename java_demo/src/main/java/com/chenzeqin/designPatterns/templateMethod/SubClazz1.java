package com.chenzeqin.designPatterns.templateMethod;

public class SubClazz1 extends AbstractClazz{
    @Override
    void templateMethod() {
        System.out.println("子类1具体的实现");
    }
}
