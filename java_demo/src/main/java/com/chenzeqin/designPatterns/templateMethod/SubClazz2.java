package com.chenzeqin.designPatterns.templateMethod;

public class SubClazz2 extends AbstractClazz{
    @Override
    void templateMethod() {
        System.out.println("子类2具体的实现");
    }
}
