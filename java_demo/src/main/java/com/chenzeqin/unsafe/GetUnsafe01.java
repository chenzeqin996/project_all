package com.chenzeqin.unsafe;

import sun.misc.Unsafe;

/**
 * @author chenzeqin
 * @menu
 * @date 2021-1-9
 */
public class GetUnsafe01 {
    public static void main(String[] args) {
        Unsafe unsafe = Unsafe.getUnsafe();
        System.out.println("获取Unsafe类:" + unsafe);
    }
}
