package com.chenzeqin.unsafe.unsafeCreateObject;

/**
 * @author chenzeqin
 * @menu
 * @date 2021-1-9
 */
public class Obj {
    private long a;

    public Obj(){
        System.out.println("调用Obj无参构造方法！");
        this.a = 1;
    }

    public long getA() {
        return a;
    }

    public void setA(long a) {
        this.a = a;
    }
}
