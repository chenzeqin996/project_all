package com.chenzeqin.unsafe.unsafeCreateObject;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * @author chenzeqin
 * @menu
 * @date 2021-1-9
 */
public class CreateObjTest {
    private static Unsafe unsafe = reflectGetUnsafe();
    public static Unsafe reflectGetUnsafe(){
        try {
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            return (Unsafe) theUnsafe.get(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        Obj o1 = new Obj();
        System.out.println("new方法创建对象："+o1.getA());

        Obj o2 = Obj.class.newInstance();
        System.out.println("反射创建对象："+o1.getA());

        Obj o3 = (Obj) unsafe.allocateInstance(Obj.class);
        System.out.println("unsafe创建对象："+o1.getA());
    }
}
