package com.chenzeqin.unsafe;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * @author chenzeqin
 * @menu
 * @date 2021-1-9
 */
public class GetUnsafe02 {
    private static Unsafe reflectGetUnsafe() {
        try {
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            return (Unsafe) field.get(null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) {
        Unsafe unsafe = reflectGetUnsafe();
        System.out.println("获取Unsafe类:" + unsafe);
    }
}
