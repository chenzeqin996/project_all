package com.chenzeqin.multiThread;

/**
 * 死锁案例
 */
public class DeadLockDemo {
    private volatile static Integer source1 = new Integer(1);
    private volatile static Integer source2 = new Integer(1);
    public static void main(String[] args) {
        new Thread(()->{
            synchronized (source1){
                System.out.println("线程1获取到source1资源"+source1);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("线程1开始获取source2资源"+source2);
                synchronized (source2){
                    System.out.println("线程1获取到source2资源"+source2);
                }
            }
        }).start();

        new Thread(()->{
            synchronized (source2){
                System.out.println("线程2获取到source2资源"+source2);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("线程2开始获取source1资源"+source1);
                synchronized (source1){
                    System.out.println("线程1获取到source1资源"+source1);
                }
            }
        }).start();
    }
}
