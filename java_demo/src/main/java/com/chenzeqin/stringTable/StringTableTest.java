package com.chenzeqin.stringTable;

public class StringTableTest {

    public static void main(String[] args) {
        // 例子1：下面语句创建几个对象：1个或2个对象
        // 如果字符串常量池中不存在hello
        String str1 = new String("hello");
        StringBuilder helloworld = new StringBuilder("helloworld");
        StringBuilder sb = new StringBuilder().append("hello").append("world");
        System.out.println(sb);
    }
}
