package com.chenzeqin.objLayout;

import org.openjdk.jol.info.ClassLayout;

public class ObjLayoutTest {
    public static void main(String[] args) {
        Object obj = new Object();
        System.out.println(ClassLayout.parseInstance(obj).toPrintable());
    }
}
