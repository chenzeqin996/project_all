package com.chenzeqin.wait_notify_notifyAll;

public class Test {
    private int num = 0;

    // wait、notify机制是在java 的0bject对象里面的
    // wait:就是让当前线程释放锁，保存运行状态，然后进入等待状态
    // notify:就是唤醒当前处于等待状态的一个线程，具体唤醒哪个线程时不确定的
    // notifyAll:就是唤醒当前所有处于等待状态的线程
    public synchronized void setNum(){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("setNum thread:"+Thread.currentThread().getName());
        num++;
        if(num==2){
            notify();   // 唤醒一个等待中的线程
            // notifyAll();// 唤醒所有等待中的线程
        }
    }
    public synchronized int getNum(){
        System.out.println("getNum thread:"+Thread.currentThread().getName());
        if(num < 2){
            try {
                wait(); // 等待唤醒
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        return num;
    }

    public static void main(String[] args) {
        Test t = new Test();
        new Thread(()->{
            t.setNum();
        }).start();
        new Thread(()->{
            t.setNum();
        }).start();
        new Thread(()->{
            System.out.println("线程3获取的结果："+t.getNum());
        }).start();
        new Thread(()->{
            System.out.println("线程4获取的结果："+t.getNum());
        }).start();
    }
}
