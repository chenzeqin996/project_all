package com.chenzeqin.java8.functionalInterface;

@FunctionalInterface
public interface AFun<T> {

    void sysout(T str);
    // void equal(Object a);
}
