package com.chenzeqin.synchronize;

import org.openjdk.jol.info.ClassLayout;

/**
 * 偏向锁测试
 */
public class BiasedLockTest {
    private static Object obj = new Object();
    public static void main(String[] args) {
        new Thread(()->{
            for (int i = 0; i < 5; i++) {
                // ...
                System.out.println(ClassLayout.parseInstance(obj).toPrintable());
            }
        }).start();
    }
}
