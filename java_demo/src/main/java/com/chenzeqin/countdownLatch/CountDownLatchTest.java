package com.chenzeqin.countdownLatch;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchTest {

    /**
     * 模拟场景：像王者荣耀，需要凑齐10个玩家准备完毕，才开始游戏。
     * 10个玩家就相当于10个线程，一个玩家准备完毕则计数器减一，只有当减为0时 主线程才继续往下执行开始游戏
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        int num = 10;
        // 初始化一个倒计时器，倒计时10个数
        CountDownLatch cd = new CountDownLatch(10);
        for (int i = 1; i <= 10; i++) {
            int finalI = i;
            new Thread(()->{
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("玩家"+ finalI +"准备完毕");
                // 当一个线程处理完毕，则计数器减一
                cd.countDown();
            }).start();
        }

        // 调用await方法，当前线程会被阻塞，直到计数器为0才被唤醒
        cd.await();

        System.out.println("10个玩家都准备完毕，游戏开始...");
        // 执行程序...
    }
}
