package com.chenzeqin.threadPool;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolTest {

    public static void main(String[] args) {
        // 参数分别为：核心线程大小， 线程池最大大小， 线程存活保持时间，时间单位， 阻塞队列， 线程池工厂， 拒绝策略
        ThreadPoolExecutor executor = new ThreadPoolExecutor(10, 20,
                0, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(), Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());

        // 可以通过这种方式，设定当核心线程数超时时间没有任务处理时，也可以被销毁
        // executor.allowCoreThreadTimeOut(true);

        // lambda表达式执行
        executor.execute(()->{
            System.out.println("业务流程1");
        });

        // 匿名内部类方式执行
        executor.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("业务流程2");
            }
        });

        executor.shutdown();
        executor.shutdownNow();
    }
}
