package com.chenzeqin.queue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class QueueTest {

    /**
     * 队列使用
     * @param args
     */
//    public static void main(String[] args) {
//        Queue queue = new LinkedList();
//        queue.offer("1");
//        queue.offer("2");
//        queue.offer("3");
//
//        System.out.println(queue.poll());
//        System.out.println(queue.poll());
//        System.out.println(queue.poll());
//    }


    /**
     * 阻塞队列使用
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue queue = new LinkedBlockingQueue();

        Thread producerThread = new Thread(()->{
            for(int i=0; i<5 ; i++){
                queue.offer("商品"+i);
                System.out.println("放入商品"+i);
            }
        });

        Thread consumerThread = new Thread(()->{
            for(int i=0; i<5 ; i++){
                try {
                    // 一直等待，直到从队列中拿到数据
                    // Object data = queue.take();
                    // 等待指定时间，拿不到则抛出异常
                    Object data = queue.poll(2, TimeUnit.SECONDS);
                    // 不等待，获取不到则返回null
                    // Object data = queue.poll();
                    if(data==null){
                        System.out.println("没有获取到商品");
                    }else{
                        System.out.println("获取到"+data);
                    }
                } catch (InterruptedException e) {
                    System.out.println("没有获取到商品");
                    throw new RuntimeException(e);
                }
            }
        });

        producerThread.start();
        Thread.sleep(10);
        consumerThread.start();
    }
}
