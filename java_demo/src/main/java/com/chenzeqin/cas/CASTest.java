package com.chenzeqin.cas;

import java.util.concurrent.atomic.AtomicInteger;

public class CASTest {
    private static AtomicInteger i = new AtomicInteger();

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            for (int j = 0; j < 100; j++) {
                i.incrementAndGet();
            }
        });
        Thread thread2 = new Thread(()->{
            for (int j = 0; j < 100; j++) {
                i.incrementAndGet();
            }
        });
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();

        System.out.println("2个线程各累加100次后i的结果为:"+i.get());
    }
}
