package Semaphore;

import java.util.concurrent.Semaphore;

public class SemaphoreTest {
    /**
     * 模拟场景：停车场；
     * 停车场内有10个车位，当有车(线程)进行则会占用车位，当10个车位被占满，则后面的车就只能在外面等待
     * 当停车场内的车出来了之后，则释放停车位。外面等待的车就能进来
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(10);


        new Thread(()->{
            System.out.println("1车-请求进场");
            try {
                semaphore.acquire();
                System.out.println("1车-进场");
                Thread.sleep(10000l);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("1车-出场");
            semaphore.release();
        }).start();
        Thread.sleep(1000l);

        new Thread(()->{
            System.out.println("9辆车-请求进场");
            try {
                semaphore.acquire(9);
                System.out.println("9辆车-进场");
                Thread.sleep(20000l);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("9辆车-出场");
            semaphore.release(9);
        }).start();
        Thread.sleep(1000l);

        new Thread(()->{
            System.out.println("11辆车-请求进场");
            try {
                semaphore.acquire(1);
                System.out.println("11辆车-进场");
                Thread.sleep(5000l);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("11辆车-出场");
            semaphore.release(1);
        }).start();
    }
}
