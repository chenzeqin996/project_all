package com.chenzeqin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: chenzeqin
 * @date: 2022/4/9
 */
@Controller
// @RequestMapping("/test")  // 类上也可以写上RequestMapping，例如@RequestMapping("/test")，最终该controller的所有请求地址将加上/test
public class HelloController {

    @RequestMapping("/hello")
    public String hello(Model model){
        // Model对应可以向模型中添加属性msg与值，可以在JSP页面中取出并渲染
        model.addAttribute("msg", "hello springmvc annotation");
        // return直接返回视图，将交由视图解析器处理，跳转至对应的页面
        return "hello";
    }
}
