package com.chenzeqin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 测试中文乱码
 * @author: chenzeqin
 * @date: 2022/4/9
 */
@Controller
public class EncodeController {

    @RequestMapping("/encode/test")
    public String test(Model model, String text){
        model.addAttribute("msg", text);
        return "hello";
    }
}
