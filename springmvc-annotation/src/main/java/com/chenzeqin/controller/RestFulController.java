package com.chenzeqin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * restful风格代码
 * @author: chenzeqin
 * @date: 2022/4/9
 */
@Controller
public class RestFulController {

    /**
     * 传统方式：查询请求
     * 请求方式(get/post) http:localhost:8080/test/get?id=1
     * @RequestMapping 默认支持get和post两种请求
     */
    @RequestMapping("/test/get")
    public String get(Model model, String id){
        model.addAttribute("msg", "get id:" + id);
        return "hello";
    }
    
    /**
     * 传统方式：删除请求
     * 请求方式(get/post) http:localhost:8080/test/delete?id=1
     */
    @RequestMapping("/test/delete")
    public String delete(Model model, String id){
        model.addAttribute("msg", "delete id:" + id);
        return "hello";
    }

    /**
     * restful风格：查询请求
     * 请求方式(get) http:localhost:8080/test/1
     */
    @RequestMapping(value = "/test/{id}", method = RequestMethod.GET)
    // @GetMapping("/test/{id}")
    public String restget(Model model, String id){
        model.addAttribute("msg", "get id:" + id);
        return "hello";
    }

    /**
     * restful风格：删除请求
     * 请求方式(delete) http:localhost:8080/test/1
     */
    @RequestMapping(value = "/test/{id}", method = RequestMethod.DELETE)
    // @DeleteMapping("/test/{id}")
    public String restdelete(Model model, String id){
        model.addAttribute("msg", "delete id:" + id);
        return "hello";
    }
}
