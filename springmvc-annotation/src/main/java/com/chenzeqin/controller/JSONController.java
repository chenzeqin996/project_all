package com.chenzeqin.controller;

import com.alibaba.fastjson.JSON;
import com.chenzeqin.dto.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * json格式传递数据
 * @author: chenzeqin
 * @date: 2022/4/9
 */
@RestController
public class JSONController {

    @RequestMapping("/json1")
    // @ResponseBody
    public User json1(){
        User user = new User(1, "小明");
        return user;
    }

    @RequestMapping("/json2")
    public String json2() throws JsonProcessingException {
        User user = new User(1, "小明");
        return new ObjectMapper().writeValueAsString(user);
    }

    @RequestMapping("/json3")
    public String json3() throws JsonProcessingException {
        User user = new User(1, "小明");
        return JSON.toJSONString(user);
    }
}
